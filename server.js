const express = require('express');
const app = express();
const DB = require('./config/database');
const productRouter = require('./api/product/product.router');
require("dotenv").config();

DB();
//Connecting with DB
app.use(express.json());
app.use("/api/p", productRouter);

app.listen(process.env.PORT, () => {
    console.log("Server is listening on port "+process.env.PORT);
});