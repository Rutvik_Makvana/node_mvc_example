const productcontroller = require('./product.controller');
const router = require('express').Router();

router.post('/add', productcontroller.create);

router.get('/show_all', productcontroller.list);

router.get('/show_one/:id', productcontroller.getProductById);

router.put('/update', productcontroller.updateProduct);

router.delete('/delete', productcontroller.deleteProduct);

module.exports = router;