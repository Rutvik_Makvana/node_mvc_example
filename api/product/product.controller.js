const { list } = require('./product.service');
const productService = require('./product.service');

module.exports = {
    create : (req, res) => {
        const body = req.body;
        productService.create(body).then(result => {
            if(result){
                res.json({
                    success : 1,
                    message : "Data Inserted Successfully",
                    data : result
                })
            }
            else{
                res.json({
                    success : 0,
                    message : "Failed to Insert Data...",
                })
            }
        })
    },

    list : (req, res) => {
        productService.list().then(result => {
            if(result){
                res.json({
                    success : 1,
                    data : result
                })
            }
            else{
                res.json({
                    success : 0
                })
            }
        })
    },

    getProductById : (req, res) => {
        const id = req.params.id;
        productService.getProductById(id).then(result => {
            if(result){
                res.json({
                    success : 1,
                    data : result
                })
            }
            else{
                res.json({
                    success : 0
                })
            }
        })
    },

    updateProduct : (req, res) => {
        const id = req.body._id;
        productService.updateProduct(id,req.body).then(result => {
            if(result){
                res.json({
                    success : 1,
                    message : "Data Updated Successfully",
                })
            }
            else{
                res.json({
                    success : 0,
                    message : "Failed to Update Data...",
                })
            }
        })
    },

    deleteProduct : (req, res) => {
        productService.deleteProduct(req.body).then(result => {
            if(result){
                res.json({
                    success : 1,
                    message : "Data Deleted Successfully",
                    data : result
                })
            }
            else{
                res.json({
                    success : 0,
                    message : "Failed to Delete Data...",
                })
            }
        })
    }
}